<?php

namespace Drupal\cookie_onetrust\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provide settings page for adding cookie_onetrust header scripts.
 */
class HeaderForm extends ConfigFormBase {

  /**
   * Implements FormBuilder::getFormId.
   */
  public function getFormId() {
    return 'cookie_onetrust_settings';
  }

  /**
   * Implements ConfigFormBase::getEditableConfigNames.
   */
  protected function getEditableConfigNames() {
    return ['cookie_onetrust.settings'];
  }

  /**
   * Implements FormBuilder::buildForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $header_section = $this->config('cookie_onetrust.settings')->get();

    $form['scripts'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Insert the Main Script Tag from Onetrust here. Remove the comments at the start and end of the script and then save the configuration.'),
      '#default_value' => isset($header_section['scripts']) ? $header_section['scripts'] : '',
      '#rows'          => 10,
    ];

    $form['block_notice_c003'] = [
      '#type'          => 'text_format',
      '#title'         => $this->t('Block notice(Functional Cookies)'),
      '#default_value' => isset($header_section['block_notice_c003']['value']) ? $header_section['block_notice_c003']['value'] : '<div class="one-trust-notice">Please <a href="javascript:OneTrust.ToggleInfoDisplay()">allow functional cookies</a> to view this embedded content from <a href="!cookie_onetrust_from_src_url" class="onetrust-video-link" target="_blank">!cookie_onetrust_from_src_url</a></div>',
      '#format' => 'full_text',
    ];

    $form['block_notice_c004'] = [
      '#type'          => 'text_format',
      '#title'         => $this->t('Block notice(Targeting Cookies)'),
      '#default_value' => isset($header_section['block_notice_c004']['value']) ? $header_section['block_notice_c004']['value'] : '<div class="one-trust-notice">Please <a href="javascript:OneTrust.ToggleInfoDisplay()">allow targeting cookies</a> to view this embedded content from <a href="!cookie_onetrust_from_src_url" class="onetrust-video-link" target="_blank">!cookie_onetrust_from_src_url</a></div>',
      '#format' => 'full_text',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements FormBuilder::submitForm().
   *
   * Serialize the user's settings and save it to the Drupal's config Table.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configFactory()
      ->getEditable('cookie_onetrust.settings')
      ->set('scripts', $values['scripts'])
      ->set('block_notice_c003', $values['block_notice_c003'])
      ->set('block_notice_c004', $values['block_notice_c004'])
      ->save();

    $this->messenger()->addMessage('Your Settings have been saved.', 'status');
  }

}
